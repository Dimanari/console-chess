# Console Chess #

This is a "for fun" project I'm doing. it's public for educational purposes only and is probably NOT going to be updated in the future.

### What is this repository for? ###

* a fun project to make the basics of chess programming as simple to understand as possible.
* to be a reference for my coding skills or lackthereof.
* [For detailed explenation and overview of the code](https://www.youtube.com/watch?v=Oa4r9afd3ic)

### How do I get set up? ###

* a working executable will be available in my [itch.io profile](https://dimanari.itch.io/), free with some updates and maybe actual graphics.
* the code is not supposed to be copied and used because I don't really have the energy to teach people how to copy paste .h and .cpp files into a compiler.

### Contribution guidelines ###

* Suggestions for optimizations and AI integration would be welcome.
* Job offers, colaborations and other ways to spend time are an awesome help.

### Who do I talk to? ###

* Dimanari(me) at: dimanaricodestuff@gmail.com